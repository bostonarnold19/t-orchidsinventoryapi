<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BuyerType extends Model
{
    protected $table = 'buyer_type';
}
