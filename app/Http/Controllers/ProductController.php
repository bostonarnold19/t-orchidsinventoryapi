<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::all();
        return response()->json($products);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $categories = Category::where(function ($query) use ($data) {
            $query->where('category_name', 'like', '%' . $data['query'] . '%');
        })->get();
        return response()->json($categories);
    }

    public function store(Request $request)
    {
        $product = new Product;
        $product->fill($request->all());
        $product->save();
        return response()->json([
            'message' => 'product successfully added',
        ], 200);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $product = Product::find($id);
        return response()->json($product);
    }

    public function update(Request $request, $id)
    {
        $product = Product::find($id);
        $product->update($request->all());
        $product->save();
        return response()->json([
            'message' => 'product successfully updated',
        ], 200);
    }

    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();
        return response()->json([
            'message' => 'product successfully deleted',
        ], 200);
    }
}
