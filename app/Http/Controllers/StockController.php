<?php

namespace App\Http\Controllers;

use App\Product;
use App\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public function index()
    {
        $stocks = Stock::all();
        return response()->json($stocks);
    }

    public function create(Request $request)
    {
        $data = $request->all();
        $products = Product::where(function ($query) use ($data) {
            $query->where('product_name', 'like', '%' . $data['query'] . '%');
        })->get();
        return response()->json($products);
    }

    public function store(Request $request)
    {
        $stock = new Stock;
        $arrival_date_string = strtotime($request->arrival_date);
        $arrival_date = date('Y-m-d', $arrival_date_string);
        $stock->fill($request->all());
        $stock->save();
        return response()->json([
            'message' => 'stock successfully added',
        ], 200);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $stock = Stock::find($id);
        return response()->json($stock);
    }

    public function update(Request $request, $id)
    {
        $stock = Stock::find($id);
        $arrival_date_string = strtotime($request->arrival_date);
        $arrival_date = date('Y-m-d', $arrival_date_string);
        $stock->update($request->all());
        $stock->save();
        return response()->json([
            'message' => 'stock successfully updated',
        ], 200);
    }

    public function destroy($id)
    {
        $stock = Stock::find($id);
        $stock->delete();
        return response()->json([
            'message' => 'stock successfully deleted',
        ], 200);
    }
}
