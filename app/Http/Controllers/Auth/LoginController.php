<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function login(Request $request)
    {
        $user = User::where('username', $request->username)
            ->where('password', md5($request->password))
            ->first();

        if (!empty($user)) {
            return response()->json($user, 200);
        } else {
            return response()->json([
                'error' => 'invalid_credentials',
                'message' => 'The user credentials were incorrect.',
            ], 401);
        }
    }
}
