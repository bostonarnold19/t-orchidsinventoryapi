<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    public $timestamps = false;

    public $fillable = [
        'transaction_id',
        'employee_id',
        'product_id',
        'supplier_name',
        'quantity_onhand',
        'quantity_initial',
        'cost',
        'arrival_date',
    ];
}
