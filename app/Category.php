<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $timestamps = false;

    public $fillable = [
        'category_name',
        'category_description',
    ];
}
