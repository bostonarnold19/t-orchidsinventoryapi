<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $timestamps = false;

    public $fillable = [
        'product_name',
        'product_description',
        'barcode',
        'category_id',
        'price',
    ];
}
