<?php

use Illuminate\Database\Seeder;

class BuyerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('buyer_type')->insert([
            'type_name' => 'Basic',
            'discount' => '',
        ]);
        \DB::table('buyer_type')->insert([
            'type_name' => 'Student',
            'discount' => '',
        ]);
        \DB::table('buyer_type')->insert([
            'type_name' => 'Teacher',
            'discount' => '',
        ]);
    }
}
