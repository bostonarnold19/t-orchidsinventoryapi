<?php

use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('products')->insert([
            'product_name' => 'pencil',
            'product_description' => '',
            'barcode' => '2712378124',
            'category_id' => '1',
            'price' => '7',
        ]);
    }
}
