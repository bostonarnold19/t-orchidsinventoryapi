<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class StockSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \DB::table('stocks')->insert([
            'product_id' => '1',
            'employee_id' => 'John Lloyd',
            'transaction_id' => '12637112',
            'quantity_onhand' => '20',
            'quantity_initial' => '20',
            'cost' => '100',
            'arrival_date' => Carbon::now()->format('Y-m-d'),
        ]);
    }
}
